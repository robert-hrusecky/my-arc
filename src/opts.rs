use clap::{App, Arg,};

#[derive(Debug)]
pub struct Opts {
    pub clones: usize,
    pub threads: usize,
    pub implementation: String
}

pub fn parse() -> Opts {
    let matches = App::new("Concurrent Queue")
        .version("1.0")
        .author("Robert Hrusecky and Omar Jamil")
        .about("MyArc tester")
        .arg(
            Arg::with_name("clones")
                .short("c")
                .required(true)
                .takes_value(true)
                .help("number of clones"),
        )
        .arg(
            Arg::with_name("threads")
                .short("t")
                .required(true)
                .takes_value(true)
                .help("number of threads"),
        )
        .arg(
            Arg::with_name("implementation")
            .short("i")
            .required(true)
            .takes_value(true)
            .help("Implementation {rustArc, myArc}"),
        )
        .get_matches();

    Opts {
        clones: matches.value_of("clones").unwrap().parse().unwrap(),
        threads: matches.value_of("threads").unwrap().parse().unwrap(),
        implementation: matches.value_of("implementation").unwrap().parse().unwrap(),
    }
}
