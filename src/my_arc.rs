use std::ops::Deref;
use std::sync::atomic::{AtomicUsize, Ordering};

pub struct HeapCell<T> {
    data: T,
    count: AtomicUsize,
}

pub struct MyArc<T> {
    ptr: *mut HeapCell<T>,
}

impl<T> MyArc<T> {
    pub fn new(data: T) -> Self {
        let heap_cell = HeapCell {
            data,
            count: AtomicUsize::new(1),
        };
        Self {
            ptr: Box::into_raw(Box::new(heap_cell)),
        }
    }
}

impl<T> Clone for MyArc<T> {
    fn clone(&self) -> Self {
        unsafe {
            let previous = (*self.ptr).count.fetch_add(1, Ordering::SeqCst);
            if previous == 0 {
                panic!("Tried to create too many MyArc references");
            }
        }
        MyArc { ..*self }
    }
}

impl<T> Deref for MyArc<T> {
    type Target = T;
    fn deref(&self) -> &T {
        unsafe { &(*self.ptr).data }
    }
}

impl<T> Drop for MyArc<T> {
    fn drop(&mut self) {
        unsafe {
            let previous = (*self.ptr).count.fetch_sub(1, Ordering::SeqCst);
            if previous == 1 {
                Box::from_raw(self.ptr);
            }
        }
    }
}

unsafe impl<T: Send + Sync> Send for MyArc<T> {}
unsafe impl<T: Send + Sync> Sync for MyArc<T> {}

#[cfg(test)]
mod tests {
    use super::*;
    use std::thread;

    #[test]
    fn basic() {
        let num = MyArc::new(1);
        let num2 = num.clone();
        let num3 = MyArc::new(1);
        assert_eq!(*num, *num2);
        assert_eq!(*num2, *num3);
        // they should be dropped here
    }

    #[test]
    fn multithread() {
        let num = MyArc::new(1);
        let num2 = num.clone();
        let handle = thread::spawn(move || {
            // num2 moved into thread stack
            assert_eq!(*num2, 1);
            // num2 dropped here
        });
        handle.join().unwrap();
        // num dropped here
    }
}
