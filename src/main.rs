mod my_arc;
mod opts;

use my_arc::MyArc;
use std::sync::Arc;
use crossbeam::thread;
use std::time::Instant;

fn main() {
    let opts = opts::parse();
    
    let start = Instant::now();
    match opts.implementation.as_str() {
        "myArc" => my_arc(opts.threads, opts.clones),
        "rustArc" => rust_arc(opts.threads, opts.clones),
        _ => {
            println!("Unknown arc implementation");
            return;
        }
    }
    let end = Instant::now();
    let duration = end - start;
    println!("{}", duration.as_nanos());
}


fn my_arc(threads: usize, clones: usize) {

    // MyArc Implementation
    let x = MyArc::new(1);

    thread::scope(|s| {
        let _: Vec<_> = (0..threads)
            .map(|_| {
                let x2 = x.clone();
                s.spawn(move |_| {
                    for _ in 0..clones {
                        let _ = x2.clone();
                    }
                })
            })
            .collect();
        
    })
    .unwrap();
}

fn rust_arc(threads: usize, clones: usize) {
    // Rust Arc Implementation
    let x = Arc::new(1);

    thread::scope(|s| {
        let _: Vec<_> = (0..threads)
            .map(|_| {
                let x2 = x.clone();
                s.spawn(move |_| {
                    for _ in 0..clones {
                        let _ = x2.clone();
                    }
                })
            })
            .collect();

    })
    .unwrap();
}
