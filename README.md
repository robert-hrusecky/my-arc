# my-arc

A homegrown minimal implementation of `Arc<T>` in Rust by __Omar Jamil__ and __Robert
Hrusecky__ for CS378H Concurrency at the University of Texas at Austin.

## Interesting links

[Rustonomicon](https://doc.rust-lang.org/nomicon/)

[Send and Sync Traits](https://doc.rust-lang.org/nomicon/send-and-sync.html)

[Atomics](https://doc.rust-lang.org/nomicon/atomics.html)

[Book Section on Smart Pointers](https://doc.rust-lang.org/stable/book/ch15-00-smart-pointers.html)

[Arc<T> Docs, API](https://doc.rust-lang.org/stable/std/sync/struct.Arc.html)

[Lol](https://doc.rust-lang.org/nomicon/arc-and-mutex.html)
