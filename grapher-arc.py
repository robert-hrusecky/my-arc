from argparse import ArgumentParser
import matplotlib.pyplot as plt
import os
import subprocess
import re
from itertools import repeat

def exec_profile_rust(threads, clones, impl):
    args = [
        "./my-arc/target/release/my-arc",
        "-t", str(threads),
        "-c", str(clones),
        "-i", impl,
    ]
    print(" ".join(args))
    completed = subprocess.run(args, capture_output=True, text=True)
    return int(completed.stdout) / 1000000000

def exec_avg(f, n, threads, clones, impl):
    sum = 0.0
    for _ in range(n):
        sum += f(threads, clones, impl)
    return sum / n

def line_graph(title, xlabel, ylabel, n, threads, clones, impls, labels):
    out_f = "report/{}.png".format(title.lower().replace(" ", "_"))
    print("==== Getting data for:", title, "====")

    fig, ax = plt.subplots()

    for impl in impls:
        time = []
        for t in threads:
            time.append(exec_avg(exec_profile_rust, n, t, clones, impl))
        ax.plot(threads, time)
    
    plt.legend(labels, loc="upper left")
    plt.xticks(threads)
    ax.set(xlabel=xlabel, ylabel=ylabel, title=title)
    ax.set_ylim(bottom=0)
    ax.grid()
    fig.savefig(out_f)

if __name__  == "__main__":
    line_graph(
        "Arc Implementation vs Time",
        "Implementation",
        "Time (s)",
        10,
        threads=[i for i in range(1, 21)],
        clones=100000,
        impls=["myArc", "rustArc"],
        labels=["MyArc", "RustArc"]
    )
